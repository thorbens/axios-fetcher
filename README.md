# Axios Fetcher

## Description
This package implements a [fetcher](https://www.npmjs.com/package/@thorbens/fetcher-model) using the [axios](https://www.npmjs.com/package/axios) library.

## Use
```typescript
const fetcher = new AxiosFetcher();
const response = await fetcher.fetch("http://google.com");
const body = response.body; // contains the raw response body
```

Post example:
```typescript
const fetcher = new AxiosFetcher();
const options: FetchOptions = {
    data: {
        foo: `bar`,
    },
    method: Method.POST,
};
const response = await fetcher.fetch("https://httpbin.org/post", options);
const json = response.asJSON<MyResponseObject>(); // contains the data returned as json object
```
