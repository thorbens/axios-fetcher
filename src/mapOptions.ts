import {FetchOptions} from "@thorbens/fetcher-model";
import {AxiosRequestConfig} from "axios";

/**
 * Maps the given options to fetch options.
 *
 * @param options The options to map.
 */
export function mapOptions(options: FetchOptions): AxiosRequestConfig {
    const requestConfig: AxiosRequestConfig = {
        method: options.method || `get`,
        // disable json parsing, @see https://github.com/axios/axios/issues/907#issuecomment-373988087
        transformResponse: undefined,
        validateStatus: () => true,
    };
    if (options.headers) {
        requestConfig.headers = options.headers;
    }
    if (options.data) {
        requestConfig.data = options.data;
        requestConfig.headers = {
            "Content-Type": "application/json",
            ...requestConfig.headers, // merge with existing headers
        };
    }

    return requestConfig;
}
