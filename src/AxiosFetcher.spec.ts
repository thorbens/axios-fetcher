import {FetchOptions, Method} from "@thorbens/fetcher-model";
import {AxiosFetcher} from "./AxiosFetcher";

interface HttpbinResponse {
    form: {
        foo: string,
    };
    data: string;
}

const fetcher = new AxiosFetcher();

describe("AxiosFetcher", () => {

    it("should fetch the given url", async () => {
        const response = await fetcher.fetch("http://google.com");

        expect(!!response).toEqual(true);
        expect(response.status).toEqual(200);
    });

    it("should perform a post to the given url", async () => {
        const options: FetchOptions = {
            data: {
                foo: "bar",
            },
            method: Method.POST,
        };
        const response = await fetcher.fetch("https://httpbin.org/post", options);

        expect(!!response).toEqual(true);
        expect(response.status).toEqual(200);

        const json = response.asJson<HttpbinResponse>();
        expect(json.data).toEqual(JSON.stringify(options.data));
    });

    it("should reject not successful requests", async () => {
        let hasError = false;

        try {
            await fetcher.fetch("https://httpbin.org/status/404");
        } catch (e) {
            expect(!!e).toEqual(true);
            expect(e.status).toEqual(404);
            hasError = true;
        }

        expect(hasError).toEqual(true);
    });

    it("should set the headers for the request", async () => {
        const headers = {
            Foo: "bar",
        };
        const response = await fetcher.fetch("https://httpbin.org/headers", {
            headers,
        });

        expect(!!response).toEqual(true);
        const json: any = response.asJson();
        expect(json.headers.Foo).toEqual("bar");
    });

    it("should reject an error on non resolvable hosts", async () => {
        expect.assertions(1);
        await expect(fetcher.fetch("https://ajciodsapcjmpdscijdps.xxx")).rejects.not.toEqual(null);
    });
});
