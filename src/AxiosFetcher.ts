import {FetchOptions, Response} from "@thorbens/fetcher-model";
import axios, {AxiosRequestConfig} from "axios";
import {mapOptions} from "./mapOptions";

/**
 * Axios Implementation for a Fetcher.
 *
 * @see https://www.npmjs.com/package/axios
 */
export class AxiosFetcher {
    /**
     * @inheritDoc
     */
    public fetch(url: string, options?: FetchOptions): Promise<Response> {
        return this.performRequest(url, mapOptions(options || {}));
    }

    /**
     * Performs a request to the given url with the given options.
     *
     * @param url The url to fetch.
     * @param options The options to use.
     */
    private async performRequest(url: string, options: AxiosRequestConfig): Promise<Response> {
        const res = await axios.request({
            ...options,
            transformResponse: r => r,
            url,
        });
        const contentType = res.headers["content-type"] || null;
        const statusCode = res.status;
        const body = await res.data;
        const response = this.createResponse(body, statusCode, contentType);
        if (response.status >= 400) {
            return Promise.reject(response);
        }

        return response;
    }

    private createResponse(body: string | undefined, status: number, contentType: string | undefined): Response {
        return {
            asJson: () => body ? JSON.parse(body) : undefined,
            body,
            contentType,
            status,
        };
    }
}
